Rails.application.routes.draw do
  get 'mypage/show'
  devise_for :users
  root 'pages#index'
  get 'pages/show'
  get 'terms', to: 'pages#terms'
  get 'policy', to: 'pages#policy'
  get 'questions', to: 'questions#index'
  resources 'profiles' do
    resources 'experiences'
    resources 'introductions'
    resources 'interviews'
    resources 'questions' do
      resources 'answers'
    end
  end
  get 'admin', to: 'admin#index'
  namespace 'admin' do
    resources 'profiles'
    resources 'experiences'
    resources 'questions'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
