module ApplicationHelper
    def errmessage(model, fldName)
        if model.errors.messages[fldName.to_sym].any?
            model.errors.full_messages_for(fldName.to_sym).each do |msg|
              return "<div class=field_with_errors>#{msg}</div>"
            end
        end
    end

    def devise_error_messages
        #return "" if resource.errors.empty?
        html = ""
        # エラーメッセージ用のHTMLを生成
        messages = resource.errors.full_messages.each do |msg|
          html += <<-EOF
            <li class="alert-alert">
              #{msg}
            </li>
          EOF
        end
        html.html_safe
    end
end