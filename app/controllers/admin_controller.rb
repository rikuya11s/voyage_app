class AdminController < ApplicationController
  layout 'admin'
  before_action :admin_required

  def index
    @profiles = Profile.all
  end

  private

  def admin_required
    redirect_to root_path if !current_user || !current_user&.admin?
  end
end
