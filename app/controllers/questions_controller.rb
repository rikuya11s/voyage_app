class QuestionsController < ApplicationController
  before_action :login_required
  before_action :set_question, only: %i[new show edit update destroy]
  before_action :set_profile, only: %i[new show create edit update]
  before_action :raise_unless_own_question, only: %i[edit update destroy]

  def index
    @questions = Question.all
  end

  def show
    # @questioned_question = Question.find_by(questioned_id: params[:profile_id])
    @answers = Answer.where(question_id: @question.id)
  end

  def new
    redirect_to root_path if @profile&.id == current_user.id
    @question = Question.new
  end

  def create
    @question = Question.new(question_params)
    @question.questioned_id = @profile.id
    @question.questioner_id = current_user.profile.id if current_user
    if @question.save
      redirect_to profile_question_url(@profile, @question), notice: '質問を作成しました'
    else
      render :new
    end
  end

  def edit
    @question = Question.find_by(id: params[:id])
  end

  def update
    if @question.update(question_params)
      redirect_to profile_url(@question.questioned_id), notice: '質問を更新しました'
    else
      render :edit
    end
  end

  def destroy
    if @question.destroy
      redirect_to profile_url(@question.questioned_id), notice: '質問を削除しました'
    else
      redirect_to profile_url(@question.questioned_id), notice: '質問を削除に失敗しました'
    end
  end

  private

  def login_required
    redirect_to root_path unless current_user
  end

  def raise_unless_own_question
    redirect_to root_path unless current_user.present? && @question&.questioner_id == current_user.id
  end

  def set_profile
    @profile = Profile.find_by(id: params[:profile_id])
  end

  def set_question
    @question = Question.find_by(id: params[:id])
  end

  def question_params
    params.require(:question).permit(
      %i[questioner_id questioned_id title description created_at updated_at]
    )
  end
end
