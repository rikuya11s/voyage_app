class Admin::ExperiencesController < AdminController
  def index
    @experiences = Experience.all
  end

  def new
    @experience = Experience.new
  end

  def create
    @experience = Experience.new(experience_params)
    @experience.profile_id = current_user.id if current_user
    if @experience.save
      redirect_to profile_url(@experience.profile_id), notice: '経験を作成しました'
    else
      render :new
    end
  end

  def edit
    @experience = Experience.find_by(id: params[:id])
  end

  def update
    if @experience.update(experience_params)
      redirect_to profile_url(@experience.profile_id), notice: '経験を更新しました'
    else
      render :edit
    end
  end

  def destroy
    if @experience.destroy
      redirect_to profile_url(@experience.profile_id), notice: '経験を削除しました'
    else
      redirect_to profile_url(@experience.profile_id), notice: '経験を削除に失敗しました'
    end
  end
end
