class Admin::QuestionsController < AdminController
  def index
    @questions = Question.all
  end

  def show
    @answers = Answer.where(question_id: @question.id)
  end

  def new
    redirect_to root_path if @profile&.id == current_user.id
    @question = Question.new
  end

  def create
    @question = Question.new(question_params)
    @question.questioned_id = @profile.id
    @question.questioner_id = current_user.profile.id if current_user
    if @question.save
      redirect_to profile_question_url(@profile, @question), notice: '質問を作成しました'
    else
      render :new
    end
  end

  def edit
    @question = Question.find_by(id: params[:id])
  end

  def update
    if @question.update(question_params)
      redirect_to profile_url(@question.questioned_id), notice: '質問を更新しました'
    else
      render :edit
    end
  end

  def destroy
    if @question.destroy
      redirect_to profile_url(@question.questioned_id), notice: '質問を削除しました'
    else
      redirect_to profile_url(@question.questioned_id), notice: '質問を削除に失敗しました'
    end
  end
end
