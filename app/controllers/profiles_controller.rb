class ProfilesController < ApplicationController
  before_action :login_required
  before_action :set_profile, only: %i[show new create edit update]

  def index
    @q = Profile.ransack(params[:q])
    @profiles = @q.result(distinct: true).sort_by { rand }
  end

  # rubocop:disable Metrics/AbcSize
  def show
    @profile = Profile.find_by(user_id: params[:id])
    @experiences = Experience.where(profile_id: params[:id]).order('period_from DESC').paginate(page: params[:page], per_page: 4)
    @introduced_introductions = Introduction.where(introduced_id: params[:id])
    @introducer_introductions = Introduction.where(introducer_id: params[:id])
    @questioner_questions = Question.where(questioner_id: params[:id])
    @questioned_questions = Question.where(questioned_id: params[:id])
    @response = Response.find_by(interview_id: 1, profile_id: params[:id])
  end
  # rubocop:enable Metrics/AbcSize

  def new
    @profile = Profile.new
    @profile.profile_course.build
  end

  def create
    @profile = Profile.new(profile_params)
    if current_user
      @profile.id = current_user.id
      @profile.user_id = current_user.id
    end
    if @profile.save
      redirect_to profile_url(@profile.user_id), notice: 'プロフィールを作成しました'
      notify_create_profile(@profile)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @profile.update(update_team_params)
      redirect_to profile_url(@profile.user_id), notice: 'プロフィールを更新しました'
    else
      render :edit
    end
  end

  def destroy; end

  private

  def login_required
    redirect_to root_path unless current_user
  end

  def set_profile
    @profile = Profile.find_by(user_id: current_user.id)
  end

  def notify_create_profile(profile)
    User.all.find_each do |user|
      next if user.email.blank? || (user == profile.user)

      ProfileMailer.notify_create_profile(profile, user.email).deliver if Rails.env.production?
    end
  end

  def profile_params
    params.require(:profile).permit(
      permit_params,
      profile_course_attributes: [:course_id]
    )
  end

  def update_team_params
    params.require(:profile).permit(
      permit_params,
      profile_course_attributes: %i[course_id _destroy id]
    )
  end

  def permit_params
    %i[user_id usertype first_name last_name nickname sex birthday school major_id graduated job_id description avatar_image facebook instagram twitter url_title url]
  end
end
