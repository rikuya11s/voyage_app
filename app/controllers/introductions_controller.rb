class IntroductionsController < ApplicationController
  before_action :login_required
  before_action :set_introduction, only: %i[new show edit update destroy]
  before_action :set_profile, only: %i[new show create edit update]
  before_action :raise_unless_own_introduction, only: %i[edit update destroy]

  def show
    @introduced_introductions = Introduction.where(introduced_id: params[:profile_id]).paginate(page: params[:page], per_page: 4)
  end

  def new
    @introduction = Introduction.new
  end

  def create
    @introduction = Introduction.new(introduction_params)
    @introduction.introduced_id = @profile.id
    @introduction.introducer_id = current_user.profile.id if current_user
    if @introduction.save
      redirect_to profile_url(@introduction.introduced_id), notice: '紹介文を作成しました'
      IntroductionMailer.notify_create_introduction(@introduction).deliver if Rails.env.production?
    else
      render :new
    end
  end

  def edit
    @introduction = Introduction.find_by(id: params[:id])
  end

  def update
    if @introduction.update(introduction_params)
      redirect_to profile_url(@introduction.introduced_id), notice: '紹介文を更新しました'
    else
      render :edit
    end
  end

  def destroy
    if @introduction.destroy
      redirect_to profile_url(@introduction.introduced_id), notice: '紹介文を削除しました'
    else
      redirect_to profile_url(@introduction.introduced_id), notice: '紹介文を削除に失敗しました'
    end
  end

  private

  def login_required
    redirect_to root_path unless current_user
  end

  def raise_unless_own_introduction
    redirect_to root_path unless current_user.present? && @introduction&.introducer_id == current_user.id
  end

  def set_profile
    @profile = Profile.find_by(id: params[:profile_id])
  end

  def set_introduction
    @introduction = Introduction.find_by(id: params[:id])
  end

  def introduction_params
    params.require(:introduction).permit(
      %i[introducer_id introduced_id description created_at updated_at]
    )
  end
end
