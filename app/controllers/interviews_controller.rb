class InterviewsController < ApplicationController
  before_action :login_required

  def login_required
    redirect_to root_path unless current_user
  end
end
