class AnswersController < ApplicationController
  before_action :login_required
  before_action :set_answer, only: %i[new show edit update destroy]
  before_action :set_question, only: %i[new show create edit update]
  before_action :set_profile, only: %i[new show create edit update]
  before_action :raise_unless_own_answer, only: %i[edit update destroy]

  def index
    @answers = Answer.all
  end

  def new
    @answer = Answer.new
  end

  def create
    @answer = Answer.new(answer_params)
    @answer.profile_id = @profile.id
    @answer.question_id = @question.id
    @answer.profile_id = current_user.profile.id if current_user
    if @answer.save
      redirect_to profile_question_url(@profile, @question), notice: '回答を作成しました'
    else
      render :new
    end
  end

  def edit
    @answer = Answer.find_by(id: params[:id])
  end

  def update
    if @answer.update(answer_params)
      redirect_to profile_url(@answer.profile_id), notice: '質問を更新しました'
    else
      render :edit
    end
  end

  def destroy
    if @answer.destroy
      redirect_to profile_url(@answer.profile_id), notice: '回答を削除しました'
    else
      redirect_to profile_url(@answer.profile_id), notice: '回答を削除に失敗しました'
    end
  end

  private

  def login_required
    redirect_to root_path unless current_user
  end

  def raise_unless_own_answer
    redirect_to root_path unless @answer&.profile_id == current_user.id
  end

  def set_profile
    @profile = Profile.find_by(id: params[:profile_id])
  end

  def set_question
    @question = Question.find_by(id: params[:question_id])
  end

  def set_answer
    @answer = Answer.find_by(id: params[:id])
  end

  def answer_params
    params.require(:answer).permit(
      %i[profile_id description created_at updated_at]
    )
  end
end
