class ExperiencesController < ApplicationController
  before_action :login_required
  before_action :set_profile, only: %i[new create edit update destroy raise_unless_own_experience]
  before_action :set_experience, only: %i[new create edit update destroy raise_unless_own_experience]
  before_action :raise_unless_own_experience, only: %i[new create edit update destroy]

  def new
    @experience = Experience.new
  end

  def create
    @experience = Experience.new(experience_params)
    @experience.profile_id = current_user.id if current_user
    if @experience.save
      redirect_to profile_url(@experience.profile_id), notice: '経験を作成しました'
    else
      render :new
    end
  end

  def edit
    @experience = Experience.find_by(id: params[:id])
  end

  def update
    if @experience.update(experience_params)
      redirect_to profile_url(@experience.profile_id), notice: '経験を更新しました'
    else
      render :edit
    end
  end

  def destroy
    if @experience.destroy
      redirect_to profile_url(@experience.profile_id), notice: '経験を削除しました'
    else
      redirect_to profile_url(@experience.profile_id), notice: '経験を削除に失敗しました'
    end
  end

  private

  def login_required
    redirect_to root_path unless current_user
  end

  def raise_unless_own_experience
    redirect_to root_path if current_user.blank? || (@profile.user_id != current_user.id)
  end

  def set_profile
    @profile = Profile.find_by(user_id: current_user.id)
  end

  def set_experience
    @experience = Experience.find_by(id: params[:id])
  end

  def experience_params
    params.require(:experience).permit(
      %i[profile_id title period_from period_to place description created_at updated_at]
    )
  end
end
