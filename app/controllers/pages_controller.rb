class PagesController < ApplicationController
  def index
    redirect_to new_profile_url if current_user && !Profile.find_by(user_id: current_user.id)
    redirect_to profile_url(current_user.id) if current_user && Profile.find_by(user_id: current_user.id)
  end

  def show; end

  def terms; end

  def policy; end
end
