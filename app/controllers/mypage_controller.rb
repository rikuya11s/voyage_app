class MypageController < ApplicationController
  before_action :set_profile, only: %i[show]

  def show; end

  private

  def set_profile
    @profile = Profile.find_by(user_id: current_user.id)
  end
end
