class User::Role < ApplicationRecord
  self.table_name = 'user_roles'

  belongs_to :user

  enum admin_role: %w[admin_all admin]

  scope :admin, -> { where(admin_role: admin_roles[:admin]) }
end
