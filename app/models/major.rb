class Major < ApplicationRecord
  def full_name
    faculty_name + department_name
  end
end
