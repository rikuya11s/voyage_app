class Course < ApplicationRecord
  belongs_to :batch
  belongs_to :country

  def full_name
    batch.name + name
  end
end
