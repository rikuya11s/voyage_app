class Answer < ApplicationRecord
  belongs_to :profile
  belongs_to :question
end
