class Question < ApplicationRecord
  belongs_to :questioner, class_name: 'Profile', foreign_key: 'questioner_id'
  belongs_to :questioned, class_name: 'Profile', foreign_key: 'questioned_id'
  has_many :answer, dependent: :destroy
  validates :description, presence: true, length: { maximum: 255 }
end
