class Experience < ApplicationRecord
  belongs_to :profile

  validates :title, presence: true, length: { maximum: 255 }
  validates :place, presence: true, length: { maximum: 255 }
  validates :description, presence: true, length: { maximum: 255 }
end
