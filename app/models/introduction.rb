class Introduction < ApplicationRecord
  belongs_to :introducer, class_name: 'Profile', foreign_key: 'introducer_id'
  belongs_to :introduced, class_name: 'Profile', foreign_key: 'introduced_id'
  validates :description, presence: true, length: { maximum: 255 }
end
