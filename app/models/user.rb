class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable, :timeoutable, :omniauthable,
         invite_for: 24.hours
  has_one :profile, dependent: :destroy
  has_one :role, dependent: :destroy

  attr_accessor :mode

  def admin?
    role.present?
  end

  def save_role(new_role)
    if role
      role.update(admin_role: new_role)
    else
      role.create_role(admin_role: new_role)
    end
  end
end
