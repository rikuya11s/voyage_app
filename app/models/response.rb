class Response < ApplicationRecord
  belongs_to :profile
  belongs_to :interview
end
