class Profile < ApplicationRecord
  belongs_to :user
  belongs_to :major
  belongs_to :job
  has_many :profile_course, dependent: :destroy
  has_many :experience, dependent: :destroy
  has_many :introducer_introduction, class_name: 'Introduction', foreign_key: 'introducer_id'
  has_many :introduced_introduction, class_name: 'Introduction', foreign_key: 'introduced_id'
  has_many :questioner_question, class_name: 'Question', foreign_key: 'questioner_id'
  has_many :questioned_question, class_name: 'Question', foreign_key: 'questioned_id'
  has_many :response, dependent: :destroy
  has_many :interview, class_name: 'Interview', foreign_key: 'interview_id'
  accepts_nested_attributes_for :profile_course, allow_destroy: true
  validates :user_id, uniqueness: true
  validates :first_name, presence: true, length: { maximum: 255 }
  validates :last_name, presence: true, length: { maximum: 255 }
  validates :nickname, presence: true, length: { maximum: 255 }
  validates :birthday, presence: true
  validates :major_id, presence: true
  validates :avatar_image, presence: true
  validates :facebook, format: /\A#{URI.regexp(%w[http https])}\z/, allow_blank: true
  validates :instagram, format: /\A#{URI.regexp(%w[http https])}\z/, allow_blank: true
  validates :twitter, format: /\A#{URI.regexp(%w[http https])}\z/, allow_blank: true
  validates :url_title, length: { maximum: 7 }
  validates :url, format: /\A#{URI.regexp(%w[http https])}\z/, allow_blank: true
  validates :description, length: { maximum: 255 }
  enum sex: { 女性: 0, 男性: 1, その他: 2 }
  enum usertype: { 学生: 0, スタッフ: 1 }
  enum graduated: { 在学: 0, 既卒: 1 }
  has_attached_file :avatar_image, PAPERCLIP_STORAGE_OPTS
  validates_attachment_content_type :avatar_image, content_type: %r{\Aimage\/.*\Z}

  def full_name
    last_name + first_name
  end
end
