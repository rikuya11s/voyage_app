class ProfileMailer < ApplicationMailer
  def notify_create_profile(profile, email)
    @profile = profile
    mail(to: email, subject: "#{@profile.nickname}さんがVoyager.appに登録しました。")
  end
end
