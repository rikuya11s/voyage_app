class IntroductionMailer < ApplicationMailer
  def notify_create_introduction(introduction)
    @introduction = introduction
    mail(to: @introduction.introduced.user.email, subject: "#{@introduction.introducer.nickname}さんがあなたの紹介文を追加しました。")
  end
end
