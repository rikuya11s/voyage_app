class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true
      t.integer :usertype
      t.string :name
      t.string :nickname
      t.integer :sex
      t.date :birthday
      t.references :major, foreign_key: true
      t.integer :graduated
      t.string :description

      t.timestamps
    end
  end
end
