class CreateUserRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :user_roles do |t|
      t.references :user, foreign_key: true
      t.integer :admin_role

      t.timestamps
    end
  end
end
