class AddColumnsProfilesUrl < ActiveRecord::Migration[5.2]
  def change
    rename_column :profiles, :instagram, :instagram
    rename_column :profiles, :twitter, :twitter
    add_column :profiles, :url_title, :string, after: :twitter
    add_column :profiles, :url, :string, after: :url_title
  end
end
