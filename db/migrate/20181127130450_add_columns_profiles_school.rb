class AddColumnsProfilesSchool < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :school, :string, after: :birthday
    add_column :profiles, :instagram, :string, after: :facebook
    add_column :profiles, :twitter, :string, after: :instagram
  end
end
