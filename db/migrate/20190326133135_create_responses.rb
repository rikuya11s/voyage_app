class CreateResponses < ActiveRecord::Migration[5.2]
  def change
    create_table :responses do |t|
      t.references :profile, foreign_key: true
      t.references :interview, foreign_key: true
      t.text :response1
      t.text :response2
      t.text :response3
      t.text :response4
      t.text :response5
      t.text :response6
      t.text :response7

      t.timestamps
    end
  end
end
