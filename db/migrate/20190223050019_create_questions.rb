class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.references :questioner
      t.references :questioned
      t.string     :title
      t.text       :description
      t.datetime   :published_at
      t.timestamps
    end
    add_foreign_key :questions, :profiles, column: :questioner_id
    add_foreign_key :questions, :profiles, column: :questioned_id
  end
end
