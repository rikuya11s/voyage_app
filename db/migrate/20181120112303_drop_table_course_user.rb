class DropTableCourseUser < ActiveRecord::Migration[5.2]
  def up
    drop_table :course_users, if_exists: true
  end

  def down
    create_table :course_users do |t|
      t.references :user, foreign_key: true
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
