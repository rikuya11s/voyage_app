class CreateInterviews < ActiveRecord::Migration[5.2]
  def change
    create_table :interviews do |t|
      t.string :title
      t.text :interview1
      t.text :interview2
      t.text :interview3
      t.text :interview4
      t.text :interview5
      t.text :interview6
      t.text :interview7

      t.timestamps
    end
  end
end
