class CreateMajors < ActiveRecord::Migration[5.2]
  def change
    create_table :majors do |t|
      t.string :faculty_name
      t.string :department_name

      t.timestamps
    end
  end
end
