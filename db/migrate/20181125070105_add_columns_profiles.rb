class AddColumnsProfiles < ActiveRecord::Migration[5.2]
  def change
    rename_column :profiles, :name, :first_name
    add_reference :profiles, :job, foreign_key: true, after: :graduated
    add_column :profiles, :last_name, :string, after: :first_name
    add_column :profiles, :facebook, :string, after: :description
  end
end
