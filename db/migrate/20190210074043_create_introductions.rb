class CreateIntroductions < ActiveRecord::Migration[5.2]
  def change
    create_table :introductions do |t|
      t.references :introducer
      t.references :introduced
      t.text :description
      t.timestamps
    end
    add_foreign_key :introductions, :profiles, column: :introducer_id
    add_foreign_key :introductions, :profiles, column: :introduced_id
  end
end
