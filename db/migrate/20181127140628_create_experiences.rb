class CreateExperiences < ActiveRecord::Migration[5.2]
  def change
    create_table :experiences do |t|
      t.references :profile, foreign_key: true
      t.string :title
      t.date :period_from
      t.date :period_to
      t.string :place
      t.string :description

      t.timestamps
    end
  end
end
