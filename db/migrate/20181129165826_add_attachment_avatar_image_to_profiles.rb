class AddAttachmentAvatarImageToProfiles < ActiveRecord::Migration[5.2]
  def self.up
    change_table :profiles do |t|
      t.attachment :avatar_image, after: :description
    end
  end

  def self.down
    remove_attachment :profiles, :avatar_image
  end
end
